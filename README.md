# Deep Learning for Natural Language Processing

Deep Learning for Natural Language Processing - Projects for the course IA376E (2020) at FEEC/Unicamp. The course uses Python3, Numpy and PyTorch for implementation. Exercises are done in Google Colab with GPU support when available.  

## Exercises:

Jupyter notebooks with solutions:

- 01 - Tokenization and Document-term matrix
- 02 - Bag of Words (BoW) - Sentiment Analysis (IMDb dataset)
- 03 - Bengio et al. Neural Probabilistic Language Model (Wikitext-2 dataset)
- 04 - Sentiment Analysis (IMDB dataset)
  - TfIdf BoW
  - Pretrained Dense Model with GloVe Embeddings
- 05 - Simplified Self-attention - Sentiment Analysis (IMDb dataset)
- 06 - Transformer Encoder - Sentiment Analysis (IMDb dataset)
  - Positional encoding
  - Scaled Dot-product Attention
  - Multi-head attention
  - Residual connections
  - LayerNorm
- 07 - Using pytorch-lightning
- 08 - BERT based model - Sentiment Analysis (IMDb dataset)
- 09 - T5 seq2seq transformer - translation EN->PT (Paracrawl)
- 10 - T5 decoder (topk, nucleus sampling, greedy)

## Summaries:

Article summaries in portuguese:

- 01 - Deep Learning
- 02 - Neural Probabilistic Language Model
- 03 - A Unified Architecture for Natural Language Processing
- 04 - Efficient Estimation of Word Representation
- 05 - Effective Approaches to Attentention-based Neural Machine Translation
- 06 - Attention Is All You Need
- 07 - BERT
- 08 - T5
- 09 - The Curious Case of Neural Text Degeneration
- 10 - Empirical Evaluation of Gated Recurrent Neural Networks on Sequence Modeling

## Final project:

Implementation of a Two Tower model for solving the problem of document retrieval (and passage ranking) in the dataset MSMarco. The project also uses queries generated using doc2query algotithm. The project is implemented using PyTorch and PyTorch Lighning, deep learning frameworks for Python.

[github repository](https://github.com/dl4nlp-rg/search-with-dense-vectors)
